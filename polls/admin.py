# coding=utf-8
from django.contrib import admin
from models import Question, Choice, Vote, TvShow
from django import forms
from django.core.exceptions import ValidationError


class QuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = "__all__"

    def clean(self):
        if self.instance.pk:
            questions = Question.objects.exclude(pk=self.instance.pk)
        else:
            questions = Question.objects.all()

        for question in questions:
            start_date = question.start_date
            end_date = question.end_date

            start_date_bool = start_date <= self.cleaned_data.get('start_date') <= end_date
            end_date_boll = start_date <= self.cleaned_data.get('end_date') <= end_date

            if start_date_bool or end_date_boll:
                raise ValidationError((u'Conflito de horários, existe uma enquete neste intervalo de tempo.'
                                      u' Enquete conflitante: ' + question.question_text).encode('utf-8'))

        return self.cleaned_data

class ChoiceInline(admin.TabularInline):
    model = Choice


class QuestionAdmin(admin.ModelAdmin):
    form = QuestionForm
    inlines = [
        ChoiceInline,
    ]

admin.site.register(Question, QuestionAdmin)
admin.site.register(TvShow)
