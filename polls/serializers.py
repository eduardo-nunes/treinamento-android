__author__ = 'weslei'

from rest_framework import serializers
from models import Question, Choice,Vote

class PollSerializer(serializers.ModelSerializer):
    start_data_formated = serializers.DateTimeField(source='start_date', format='%d/%m/%Y')
    end_data_formated = serializers.DateTimeField(source='end_date', format='%d/%m/%Y')

    class Meta:
        model = Question
        fields = ('tv_show', 'id', 'question_text', 'choices','end_data_formated','start_data_formated')
        depth = 1

class Choice(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id',)

class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ('pk',)


