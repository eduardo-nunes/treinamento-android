from rest_framework import viewsets
from serializers import PollSerializer, VoteSerializer
from models import Question, Vote, UserProfile, Choice
from rest_framework.response import Response
from rest_framework.decorators import list_route
from django.http import JsonResponse
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.utils import timezone


# create your views here.

class PollsViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = PollSerializer

    @list_route(methods=["get"])
    def get_current_poll_title_and_old_count(self, request):

        questions = Question.objects.filter(active=True)

        olds_polls_count = 0

        for question in questions:
            if timezone.now() > question.end_date:
                olds_polls_count = olds_polls_count + 1

        for question in questions:
            if question.start_date <= timezone.now() <= question.end_date:
                serializer = PollSerializer(question)
                return Response({'polls': serializer.data, 'polls_count': olds_polls_count})

        return Response({"polls_count": questions.count()})

    @list_route(methods=["get"])
    def get_current_poll(self, request):

        questions = Question.objects.filter(active=True)

        for question in questions:
            if question.start_date <= timezone.now() <= question.end_date:
                if not Vote.objects.filter(voter=request.user).filter(choice__question__id=question.id).exists():
                    serializer = PollSerializer(question)
                    return Response({'polls': serializer.data})
                else:
                    return Response({'id_question':question.id,'voted': True})

        return Response({"polls_count": questions.count()})

    @list_route(methods=["get"])
    def get_old_polls(self, request):
        # for question in questions:
        #     if timezone.now() > question.end_date:
        #         serializer = PollSerializer(question)
        #         return Response(serializer.data)

        questions = Question.objects.filter(active=True, end_date__lt=timezone.now())
        serializer = PollSerializer(questions,many=True)

        return Response(serializer.data)

    @list_route(methods=["post"])
    def vote(self, request):
        choice = Choice.objects.get(**request.data)

        vote = Vote(voter=request.user, choice=choice)
        vote.save()
        return Response({"msg": "Voto computado com sucesso", "id_poll": choice.question.id})

    @list_route(methods=["post"], permission_classes=[AllowAny])
    def get_results(self, request):
        question = Question.objects.get(id=request.data)
        choices = Choice.objects.filter(question__id=request.data)

        results = []

        for choice in choices:
            results.append({'choice_text': choice.choice_text, 'count': Vote.objects.filter(choice=choice).count()})

        return JsonResponse({'question':question.question_text, 'result': results})
