# coding=utf-8
from django.db import models
from dwsignup.models import UserProfile
from django.utils import timezone
from tv_show.models import TvShow


class Question(models.Model):
    tv_show = models.ForeignKey(TvShow,verbose_name="Programa de TV")
    question_text = models.CharField(max_length=200, verbose_name="Título enquete")
    start_date = models.DateTimeField('Data inicial')
    end_date = models.DateTimeField('Data final')
    active = models.BooleanField('Ativo', default=True)

    def __unicode__(self):
        return self.question_text

    class Meta:
        verbose_name = "Enquete"
        verbose_name_plural = "Enquetes"

class Choice(models.Model):
    question = models.ForeignKey(Question,related_name='choices')
    choice_text = models.CharField(max_length=200,verbose_name="Escolhas")

    def __unicode__(self):
        return self.choice_text

    class Meta:
        verbose_name = "Escolha"
        verbose_name_plural = "Escolhas"

class Vote(models.Model):
    voter = models.ForeignKey(UserProfile)
    choice = models.ForeignKey(Choice)
    date_voted = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.choice.choice_text

