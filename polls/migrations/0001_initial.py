# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tv_show', '0002_auto_20150624_1626'),
    ]

    operations = [
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('choice_text', models.CharField(max_length=200, verbose_name=b'Escolhas')),
            ],
            options={
                'verbose_name': 'Escolha',
                'verbose_name_plural': 'Escolhas',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_text', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo enquete')),
                ('start_date', models.DateTimeField(verbose_name=b'Data inicial')),
                ('end_date', models.DateTimeField(verbose_name=b'Data final')),
                ('active', models.BooleanField(default=True, verbose_name=b'Ativo')),
                ('tv_show', models.ForeignKey(verbose_name=b'Programa de TV', to='tv_show.TvShow')),
            ],
            options={
                'verbose_name': 'Enquete',
                'verbose_name_plural': 'Enquetes',
            },
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_voted', models.DateTimeField(default=django.utils.timezone.now)),
                ('choice', models.ForeignKey(to='polls.Choice')),
                ('voter', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(related_name='choices', to='polls.Question'),
        ),
    ]
