__author__ = 'weslei'

from rest_framework.response import Response
from models import Photos
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def post_photo(request):
        photo = Photos(user=request.user, url_image=request.data["url_image"], descricao=request.data["descricao"])
        photo.save()
        return Response("Salvo com sucesso")
