# coding=utf-8
from django.db import models
from django.utils import timezone
from dwsignup.models import UserProfile


class Photos(models.Model):
    url_image = models.CharField("Imagem", max_length=240)
    descricao = models.TextField("Descricão")
    added = models.DateTimeField("Data", default=timezone.now)
    user = models.ForeignKey(UserProfile)

    def _get_username(self):
        return '%s' % (self.user.username)

    get_username = property(_get_username)

    def _get_email(self):
        return '%s' % (self.user.email)

    get_email = property(_get_email)

    def image_tag(self):
        return u'<img width="360" src="%s" />' % self.url_image
    image_tag.short_description = 'Imagem'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = "Foto"
        verbose_name_plural = "Fotos"

    def __unicode__(self):
        return self.descricao
