# coding=utf-8
from django.contrib import admin
from models import Photos

class PhotoAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['descricao', 'image_tag', 'added']}),
        ('Informações do Usuário', {'fields': ['get_username', 'get_email']}),
    ]
    readonly_fields = ['get_username', 'descricao', 'added', 'get_email','image_tag']


admin.site.register(Photos, PhotoAdmin)
