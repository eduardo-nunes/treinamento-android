from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from dwsignup.views import UserViewSet
from polls.views import PollsViewSet
from suggestion.views import SuggestionViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
# router.register(r'polls', PollsViewSet)
# router.register(r'suggestions', SuggestionViewSet)

admin.site.site_header = 'Uni U'

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(router.urls)),

     url(r'^posts/post_photo/', 'publications.views.post_photo'),
    # url(r'^videos/post_video/', 'videos.views.post_video'),
    url(r'^sign/request_reset_password/', 'dwsignup.views.request_reset_password'),
]
