# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Suggestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'Assunto')),
                ('text', models.TextField(verbose_name=b'Sugest\xc3\xa3o')),
                ('added', models.DateTimeField(default=django.utils.timezone.now, verbose_name=b'Data')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Sugest\xe3o',
                'verbose_name_plural': 'Sugest\xf5es',
            },
        ),
    ]
