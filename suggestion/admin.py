# coding=utf-8
from django.contrib import admin
from models import Suggestion
from django import forms


class SuggestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'text', 'added']}),
        ('Informações do Usuário', {'fields': ['get_username', 'get_email','image_tag']}),
    ]
    readonly_fields = ['get_username', 'title', 'text', 'added', 'get_email','image_tag']


admin.site.register(Suggestion, SuggestionAdmin)
