__author__ = 'weslei'

from models import Suggestion
from serializers import SuggestionSerializer
from rest_framework import mixins, viewsets
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

class SuggestionViewSet(viewsets.GenericViewSet):

    queryset = Suggestion.objects.all()
    serializer_class = SuggestionSerializer

    @list_route(methods=["POST"])
    def post_suggestion(self, request):
        suggestion = Suggestion(user=request.user, text=request.data["text"], title=request.data["text"])
        suggestion.save()
        return Response("Salvo com sucesso")
