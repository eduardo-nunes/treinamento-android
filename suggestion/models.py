# coding=utf-8
from django.db import models
from django.utils import timezone
from dwsignup.models import UserProfile


class Suggestion(models.Model):
    title = models.CharField("Assunto", max_length=200)
    text = models.TextField("Sugestão")
    added = models.DateTimeField("Data", default=timezone.now)
    user = models.ForeignKey(UserProfile)

    def _get_username(self):
        return '%s' % (self.user.username)

    get_username = property(_get_username)

    def _get_email(self):
        return '%s' % (self.user.email)

    get_email = property(_get_email)

    def image_tag(self):
        return u'<img src="%s" />' % 'http://codigosimples.net/wp-content/uploads/2014/08/url-small.jpg'
    image_tag.short_description = 'Imagem'
    image_tag.allow_tags = True

    class Meta:
        verbose_name = "Sugestão"
        verbose_name_plural = "Sugestões"

    def __unicode__(self):
        return self.title
