from django.db import models

class TvShow(models.Model):
    nome = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nome

    class Meta:
        verbose_name = "Programa de TV"
        verbose_name_plural = "Programas de TV"