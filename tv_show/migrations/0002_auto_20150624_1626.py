# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tv_show', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tvshow',
            options={'verbose_name': 'Programa de TV', 'verbose_name_plural': 'Programas de TV'},
        ),
    ]
