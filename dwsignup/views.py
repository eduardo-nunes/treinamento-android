# coding=utf-8
from models import UserProfile
from rest_framework import viewsets
from serializers import UserSerializer
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import status
from django.contrib.auth import authenticate
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes


class UserViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

    @list_route(methods=["POST"], permission_classes=[AllowAny])
    def sign_in(self, request):
        if not UserProfile.objects.filter(username=request.data['email']) and not UserProfile.objects.filter(email=request.data['email']):
            user = UserProfile()
            email = request.data['email']
            password = request.data['password']
            username = request.data['email']
            user = UserProfile.objects.create_user(username,email,password)
            user.first_name = request.data['first_name']
            user.username_display = request.data['email']
            user.telefone = request.data['telefone']

            user.save()

            token = Token.objects.create(user=user)

            return JsonResponse({
                'success': True,
                'message': 'Cadastro efetuado com sucesso',
                'token': token.key
            })
        else:
            return Response("Usuario/E-mail já cadastrado",
                            status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=["POST"], permission_classes=[AllowAny])
    def sign_up(self, request):
        username = request.data['username']
        password = request.data['password']

        try:
            if UserProfile.objects.filter(username=username).exists():
                user = authenticate(username=username, password=password)
                token = Token.objects.get(user=user)

                if user:
                    return JsonResponse({
                        'success': True,
                        'message': 'Login efetuado com sucesso',
                        'token': token.key
                    })
            else:
                return Response("Usuário não encontrado", status.HTTP_400_BAD_REQUEST)
        except:
            return Response("Senha incorreta", status.HTTP_400_BAD_REQUEST)


from django.template.response import SimpleTemplateResponse
from django.core.mail import EmailMessage
def send_new_password(email, password):
    template = SimpleTemplateResponse('send_new_password.html', {'nova_senha': password})
    content = template.render()

    send_email = EmailMessage("Solicitação de Nova Senha", content.content , "nao-responda@deway.com.br" , [email])
    send_email.content_subtype = "html"
    return send_email.send()

@api_view(["POST"])
@permission_classes([AllowAny])
def request_reset_password(request):
    email = request.DATA.get('email')
    print email
    user = UserProfile.objects.get(email=email)
    password = UserProfile.objects.make_random_password(length=4, allowed_chars='0123456789')
    user.set_password(password)
    user.save()
    send_new_password(user.email, password)
    return Response({'cod': status.HTTP_200_OK, 'msg': u'E-mail enviado'})




