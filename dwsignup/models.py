from django.db import models
from django.contrib.auth.models import AbstractUser


class UserProfile(AbstractUser):
    photo = models.ImageField(upload_to='usuarioimagens/',
                              default='pic_folder/guest_user.jpg', blank=True)
    username_display = models.CharField(max_length=30)
    active = models.BooleanField(default=True)
    telefone = models.CharField(max_length=30, blank=True, null=True)