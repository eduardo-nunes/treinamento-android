# coding=utf-8
from django.db import models
from django.utils import timezone
from dwsignup.models import UserProfile


class Videos(models.Model):
    url_video = models.CharField("Video", max_length=200)
    descricao = models.TextField("Descricão")
    added = models.DateTimeField("Data", default=timezone.now)
    user = models.ForeignKey(UserProfile)

    def _get_username(self):
        return '%s' % (self.user.username)

    get_username = property(_get_username)

    def _get_email(self):
        return '%s' % (self.user.email)

    get_email = property(_get_email)

    def video_tag(self):
        return u'<iframe src="%s" height="400"></iframe>' % self.url_video
    video_tag.short_description = 'Video'
    video_tag.allow_tags = True

    class Meta:
        verbose_name = "Video"
        verbose_name_plural = "Videos"

    def __unicode__(self):
        return self.descricao
