# coding=utf-8
from django.contrib import admin
from models import Videos

class VideoAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['descricao', 'video_tag', 'added']}),
        ('Informações do Usuário', {'fields': ['get_username', 'get_email']}),
    ]
    readonly_fields = ['get_username', 'descricao', 'added', 'get_email','video_tag']


admin.site.register(Videos, VideoAdmin)
