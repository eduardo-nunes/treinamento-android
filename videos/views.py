__author__ = 'weslei'

from rest_framework.response import Response
from models import Videos
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def post_video(request):
        photo = Videos(user=request.user, url_video=request.data["url_video"], descricao=request.data["descricao"])
        photo.save()
        return Response("Salvo com sucesso")
